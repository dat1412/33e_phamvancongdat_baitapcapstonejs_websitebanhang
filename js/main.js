import {
  cartLocation,
  productLocation,
  renderCart,
  renderList,
} from "./controller/productsController.js";
import { CartItem } from "./model/productsModel.js";

const BASE_URL = "https://62f8b76ae0564480352bf5ac.mockapi.io";
let productList = [];
let cart = [];
const CART = "CART";

// Lấy dữ liệu JSON từ local storage
let cartLocalStorage = localStorage.getItem(CART);

if (JSON.parse(cartLocalStorage)) {
  // convert JSON lấy đc thành arr push vào cart
  let data = JSON.parse(cartLocalStorage);

  data.forEach((item) => {
    var cartItem = new CartItem(item.product, item.quantity);
    cart.push(cartItem);
  });
  renderCart(cart);
}

// Lưu dữ liệu lên localStorage
let saveLocalStorage = () => {
  // convert dữ liệu từ arr qua JSON
  let cartJSON = JSON.stringify(cart);

  // Luu xuống local Storage
  localStorage.setItem(CART, cartJSON);
};

// Loading
let onLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

let offLoading = () => {
  document.getElementById("loading").style.display = "none";
};

// Render dữ liệu từ axios
let renderSmartPhone = () => {
  onLoading();
  axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res);
      offLoading();
      // Render dữ liệu ra giao diện
      renderList(res.data);

      // lấy giữ liệu  push vào mảng productList
      res.data.forEach((item) => {
        productList.push(item);
      });
    })
    .catch((err) => {
      console.log("err: ", err);
      offLoading();
    });
};
renderSmartPhone();

// filter SmartPhone
let filterSmartPhone = () => {
  onLoading();
  axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res);
      offLoading();
      let product = res.data;
      let valueSelect = document
        .getElementById("filter__smartPhone")
        .value.toLowerCase();

      // filter smartPhone
      let smartPhone = product.filter((item) => {
        return item.type.toLowerCase() == valueSelect;
      });
      renderList(smartPhone);

      // all
      if (valueSelect == "all") {
        renderList(product);
      }
    })
    .catch((err) => {
      console.log("err: ", err);
      offLoading();
    });
};
window.filterSmartPhone = filterSmartPhone;

// Thêm sản phẩm vào giỏ hàng
// Theo đề bài:
let addCart = (id) => {
  let indexProduct = productLocation(productList, id);
  if (indexProduct == -1) {
    return;
  }

  let cartItem = new CartItem(productList[indexProduct], 1);

  let indexCart = cartLocation(cart, id);
  if (indexCart == -1) {
    cart.push(cartItem);
  } else {
    cart[indexCart].quantity++;
    if (cart[indexCart].quantity > 10) {
      openLimitPurchase();
      cart[indexCart].quantity = 10;
    }
  }
  saveLocalStorage();
  renderCart(cart);
};
window.addCart = addCart;

// theo cách call axios
// let addCart = (id) => {
//   onLoading();
//   axios({
//     url: `${BASE_URL}/Products/${id}`,
//     method: "GET",
//   })
//     .then((res) => {
//       console.log("res: ", res);
//       offLoading();
//       let cartItem = new CartItem(res.data, 1);
//       let indexCart = cartLocation(cart, id);
//       if (indexCart == -1) {
//         cart.push(cartItem);
//       } else {
//         cart[indexCart].quantity++;
//         if (cart[indexCart].quantity > 10) {
//           cart[indexCart].quantity = 10;
//           openLimitPurchase();
//         }
//       }
//       saveLocalStorage();
//       renderCart(cart);
//     })
//     .catch((err) => {
//       console.log("err: ", err);
//       offLoading();
//     });
// };

// Xóa sản phẩm đã chọn trọng giỏ hàng
let removeCartItem = (id) => {
  let indexCart = cartLocation(cart, id);
  if (indexCart != -1) {
    cart.splice(indexCart, 1);
    saveLocalStorage();
    renderCart(cart);
  }
};
window.removeCartItem = removeCartItem;

// Tăng số lượng sản phẩm đã chọn
let changeUp = (id) => {
  let indexCart = cartLocation(cart, id);
  if (indexCart != -1) {
    cart[indexCart].quantity++;
    if (cart[indexCart].quantity > 10) {
      closeCart();
      openLimitPurchase();
      cart[indexCart].quantity = 10;
    }
    saveLocalStorage();
    renderCart(cart);
  }
};
window.changeUp = changeUp;

// Giảm số lượng sản phẩm đã chọn
let changeDown = (id) => {
  let indexCart = cartLocation(cart, id);
  if (indexCart != -1) {
    cart[indexCart].quantity--;
    if (cart[indexCart].quantity == 0) {
      cart.splice(indexCart, 1);
    }
    saveLocalStorage();
    renderCart(cart);
  }
};
window.changeDown = changeDown;

// Xóa toàn bộ giỏ hàng
let clearCart = () => {
  cart = [];
  saveLocalStorage();
  renderCart(cart);
};
window.clearCart = clearCart;

// Mua hàng
let buy = () => {
  if (cart.length == 0) {
    renderCart(cart);
  } else {
    closeCart();
    document.querySelector(".invoice").classList.add("open");
    let totalPrices = 0;
    let contentNameHTML = "";
    let contenPriceHTML = "";
    cart.forEach((item) => {
      let contentName = `
      <span>${item.quantity} x ${item.product.name}</span>
      `;

      let contentPrice = `
      <span>$${item.totalPrice()}</span>
      `;
      totalPrices += item.totalPrice();
      contentNameHTML += contentName;
      contenPriceHTML += contentPrice;
    });

    document.getElementById("cover").classList.add("open__coverOrder");
    document.querySelector(".pay").innerHTML = `$${totalPrices}`;
    document.querySelector(
      ".shipping__item__names"
    ).innerHTML = `${contentNameHTML}`;
    document.querySelector(
      ".shipping__items__price"
    ).innerHTML = `${contenPriceHTML}`;
  }
};
window.buy = buy;

// cancel Order
let cancelOrder = () => {
  openCart();
  document.querySelector(".invoice").classList.remove("open");
  document.getElementById("cover").classList.remove("open__coverOrder");
};
window.cancelOrder = cancelOrder;

// Order
let order = () => {
  document.querySelector(".invoice").classList.remove("open");
  document.querySelector(".invoice__final").classList.add("open");
  document.querySelector(".order__id").innerHTML = `${
    Math.floor(Math.random() * 100) + 1
  } `;
  let totalPrices = 0;
  cart.forEach((item) => {
    totalPrices += item.totalPrice();
  });

  document.querySelector(".total__prices").innerHTML = `$${totalPrices}`;
};
window.order = order;

let okayInvoiceFinal = () => {
  document.querySelector(".invoice__final").classList.remove("open");
  document.querySelector(".thanks").classList.add("open");
};
window.okayInvoiceFinal = okayInvoiceFinal;

let okayFinal = () => {
  document.querySelector(".thanks").classList.remove("open");
  document.getElementById("cover").classList.remove("open__coverOrder");
  cart = [];
  saveLocalStorage();
  renderCart(cart);
};
window.okayFinal = okayFinal;
